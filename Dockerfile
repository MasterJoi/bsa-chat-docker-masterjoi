FROM node:alpine

RUN mkdir -p /masterjoi/src/react-chat
WORKDIR /masterjoi/src/react-chat

COPY package*.json /masterjoi/src/react-chat

RUN npm install

COPY . /masterjoi/src/react-chat

EXPOSE 3001 3002

CMD ["npm", "run", "dev"]