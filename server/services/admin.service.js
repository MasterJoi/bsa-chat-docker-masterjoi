const fs = require('fs');

const getUsers = () =>{
  let obj = JSON.parse(fs.readFileSync('./bin/users.json', 'utf8'));
  return obj;
};

const getUserById = (usId) =>{
  let obj = JSON.parse(fs.readFileSync('./bin/users.json', 'utf8'));
  let user = obj.find(el=>el.id===usId);
  return user;
};
const checkUser = (data) => {
  const userList = JSON.parse(fs.readFileSync('./bin/users.json', 'utf8'));
  let currentUser = userList.find((el) => el.email === data.email);
  if (currentUser){
    if(currentUser.password == data.password)
    return currentUser;
  }
  return false;
}
module.exports = {
  checkUser,
  getUsers,
  getUserById
};