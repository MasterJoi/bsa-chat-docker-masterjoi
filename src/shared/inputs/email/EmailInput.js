import React from 'react';
import PropTypes from 'prop-types';
import { Form, Alert } from 'react-bootstrap';

class EmailInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isValid: true
        };
        this.onBlur = this.onBlur.bind(this);
    }

    onBlur(e) {
        const isValid = e.target.value.includes('@');
        this.setState({
            isValid
        });
    }

    getValidationStatus() {
        return this.state.isValid;
    }

    getErrorMessage() {
        return <Alert variant="danger" className="p-1">That is not a valid email!</Alert>
    }

    render() {
        const props = this.props;
        const isValid = this.state.isValid;
        const inputClass = isValid ? 'col-sm-9' : 'error col-sm-9';

        return (
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                    value={props.text}
                    style={{ width: "100%", marginRight: "10px" }}
                    required
                    type="email"
                    placeholder="Email"
                    onChange={e => props.onChange(e, props.keyword)}
                    onBlur={this.onBlur}
                />
                {!isValid ? this.getErrorMessage() : null}
            </Form.Group>
        );
    }
}

EmailInput.propTypes = {
    text: PropTypes.string,
    type: PropTypes.string,
    keyword: PropTypes.string,
    label: PropTypes.string,
    onChange: PropTypes.func
};

export default EmailInput;