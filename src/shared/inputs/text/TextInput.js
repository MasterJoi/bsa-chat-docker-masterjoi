import React from 'react';
import PropTypes from 'prop-types';
import { Form } from 'react-bootstrap';

const TextInput = ({ text, type, keyword, label, onChange }) => {
    const onChangeEvent = e => onChange(e, keyword);
    return (
        <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>{label}</Form.Label>
            <Form.Control
                value={text}
                style={{ width: "100%" }}
                required
                type={type}
                placeholder={label}
                onChange={onChangeEvent}
            />
        </Form.Group>
    );
}

TextInput.propTypes = {
    text: PropTypes.string,
    type: PropTypes.string,
    keyword: PropTypes.string,
    label: PropTypes.string,
    onChange: PropTypes.func
};

export default TextInput;