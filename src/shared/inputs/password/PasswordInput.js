import React, { useState }from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Container } from 'react-bootstrap';

const PasswordInput = ({ text, keyword, label, onChange }) => {
    const [isShown, setIsShown] = useState(false);
    const inputType = isShown ? 'text' : 'password';

    return (
        <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Password</Form.Label>
            <Container className="d-flex p-0">
            <Form.Control
                value={text}
                    style={{ width: "100%", marginRight: "10px"}}
                required
                type={inputType}
                placeholder="Password"
                onChange={e => onChange(e, keyword)}
            />
            <Button variant="warning" className="col-sm-2" onClick={() => setIsShown(!isShown)}>&#x1f441;</Button>
            </Container>
        </Form.Group>
    );
}

PasswordInput.propTypes = {
    text: PropTypes.string,
    keyword: PropTypes.string,
    label: PropTypes.string,
    onChange: PropTypes.func
};

export default PasswordInput;