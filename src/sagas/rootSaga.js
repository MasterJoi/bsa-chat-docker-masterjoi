import { all } from 'redux-saga/effects';
import userPageSagas from '../userPage/sagas';
import usersSagas from '../users/sagas';
import logInSagas from '../login/sagas'
import chatSagas from '../chat/sagas'
import modalEditSagas from '../editMessageModal/sagas'

export default function* rootSaga() {
    yield all([
        userPageSagas(),
        usersSagas(),
        logInSagas(),
        chatSagas(),
        modalEditSagas()
    ])
};