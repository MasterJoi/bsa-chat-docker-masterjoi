import React from 'react';
import * as actions from './actions'
import { connect } from 'react-redux';
import { Container, Form } from 'react-bootstrap';
import Button from "react-bootstrap/Button";


const LoginBlockStyle = {
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: '0',
    left: '0',
    right: '0',
    display: 'flex',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    overflow: 'auto'
}

class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        this.handleLogIn = this.handleLogIn.bind(this)
    }

    componentWillReceiveProps(nextProps, prevState) {
        if (nextProps.userName !== '') {
            if (nextProps.userType === 'admin') {
                this.props.history.push('/adminPage');
            } else
                this.props.history.push('/chat');
        }
    }

    handleLogIn() {
        let userData = { email: document.getElementById('email').value, password: document.getElementById('password').value }
        this.props.loginUser(userData)
        if (this.props.userName !== '') {
            if (this.props.userType === 'admin') {
                this.props.history.push('/adminPage');
            } else
                this.props.history.push('/chat');
        }
    }

    render() {
        return (
            <Container style={ LoginBlockStyle }>
                <Form>
                    <h1 style={{ textAlign: 'center' }}>Sign In</h1>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" id="email" placeholder="Enter email" />
                        <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" id="password" placeholder="Password" />
                    </Form.Group>
                    <Button variant="primary" style={{ width: '100%' }} type="button" onClick={this.handleLogIn}>
                        Sign in!
                    </Button>
                </Form>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    console.log(state);
    return {
        userName: state.login.userName,
        userType: state.login.userType
    }
};

const mapDispatchToProps = {
    ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);