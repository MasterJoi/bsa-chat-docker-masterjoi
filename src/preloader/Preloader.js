import * as React from "react";
import { Spinner } from "react-bootstrap";

const SpinnerDivStyle = {
    display: 'flex',
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
}

const Preloader = () => {
    return <div className="preloader" style={SpinnerDivStyle}>
        <Spinner animation="grow" variant="dark" />
    </div>
}


export default Preloader;