import React, { Component } from "react";

export default class UserItem extends Component {
    render() {
        const { id, name, surname, email } = this.props;
        return (
                <div className="row align-items-center">
                <div className="col-8 d-flex justify-content-between ">
                        <span className="float-left">{name} {surname}</span>
                        <span>{email}</span>
                    </div>
                    <div className="col-4 btn-group">
                        <button className="btn btn-outline-primary" onClick={(e) => this.props.onEdit(id)}> Edit </button>
                        <button className="btn btn-outline-dark" onClick={(e) => this.props.onDelete(id)}> Delete </button>
                    </div>
                </div>
        );
    }
};